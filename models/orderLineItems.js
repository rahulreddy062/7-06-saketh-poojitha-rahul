/** 
*  
*
* author Poojitha Singam
*
*/

// bring in mongoose 
// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const OrderLineItemSchema = new mongoose.Schema({

  _id: { type: Number, required: true },
  orderLineID: {
    type: Number,
    required: true
  },
  lineNumber: {
    type: Number,
    required: true,
    default:0
  },
  productName: {
    type: String,
    required: true
  },
  quantity: {
    type: Number,
    required: true, 
    default: 1
  },
  
  price:{
     type:Number,
     required:true,
     default:0.00
 }


})
module.exports = mongoose.model('OrderLineItems', OrderLineItemSchema)
